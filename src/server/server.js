const fs = require("fs");
const path = require("path");
const uuid = require("uuid");
const http = require("http");
const url = require("url");

function readFile(filePath) {
  return new Promise((resolve, reject) => {
    fs.readFile(filePath, (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve(data);
      }
    });
  });
}

function errorHandler(err, res) {
  if (err) {
    res.writeHead(500, { "Content-Type": "application/json" });
    res.write(JSON.stringify({ error: err.message }));
    res.end();
  }
}

const server = http.createServer((req, res) => {
  let urlPath = url.parse(req.url).pathname;

  if (urlPath.includes("/delay/")) {
    // urlPath returns string value
    // slice length is based on the length of the "/delay/" string.
    let seconds = parseInt(urlPath.slice(7));

    if (typeof seconds === "number" && seconds >= 0) {
      res.writeHead(200, { "Content-type": "text/plain" });
      setTimeout(() => {
        res.end(
          `Success with ${res.statusCode} response after ${seconds} seconds`
        );
      }, seconds * 1000);
    } else {
      errorHandler(new Error("Please provide a valid postive number."), res);
    }
  } else if (urlPath.includes("/status/")) {
    // urlPath returns string value
    // slice length is based on the length of the "/status/" string.
    let status = parseInt(urlPath.slice(8));

    if (typeof status === "number" && status < 600 && status >= 100) {
      res.writeHead(status, { "Content-type": "text/html" });
      res.end(
        `<p>Status Code : ${res.statusCode}</p> <p>Status Message : ${res.statusMessage}</p>`
      );
    } else {
      errorHandler(new Error("Please enter a valid status code."), res);
    }
  } else {
    switch (req.url) {
      case "/":
        readFile(path.resolve(__dirname, "../public/index.html"))
          .then((html) => {
            res.writeHead(200, { "Content-Type": "text/html" });
            res.end(html);
          })
          .catch((err) => errorHandler(err, res));

        break;

      case "/style.css":
        readFile(path.resolve(__dirname, "../public/style.css"))
          .then((style) => {
            res.writeHead(200, { "Content-Type": "text/css" });
            res.end(style);
          })
          .catch((err) => errorHandler(err, res));

        break;

      case "/index.js":
        readFile(path.resolve(__dirname, "../public/index.js"))
          .then((js) => {
            res.writeHead(200, { "Content-Type": "text/javascript" });
            res.end(js);
          })
          .catch((err) => errorHandler(err, res));

        break;

      case "/json":
        readFile(path.resolve(__dirname, "../public/output/data.json"))
          .then((jsonData) => {
            res.writeHead(200, { "Content-Type": "application/json" });
            res.write(jsonData);
            res.end();
          })
          .catch((err) => errorHandler(err, res));

        break;

      case "/uuid":
        const uuidJs = { uuid: uuid.v4() };
        res.writeHead(200, { "Content-Type": "application/json" });
        res.end(JSON.stringify(uuidJs));

        break;

      default:
        res.writeHead(404, { "Content-Type": "application/json" });
        res.write(JSON.stringify({ error: "Page not found." }));
        res.end();

        break;
    }
  }
});

server.listen(3000);
